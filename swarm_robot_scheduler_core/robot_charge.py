# system imports
from typing import List
from threading import Thread
import time

# self imports
from swarm_robot_scheduler_core.ros_service_calls import SRSServiceCalls
from swarm_msgs.srv import RobotChargeRequest
from swarm_msgs.msg import SwarmRobotStatus

# third party imports
from nobrainer.logger.log import Log


class RobotCharge(Thread):
    """ RobotCharge module. Checks fleet battery status and send robots to charging. """

    def __init__(self, swarm_robot_scheduler: any) -> None:
        Thread.__init__(self)
        from swarm_robot_scheduler_core.swarm_robot_scheduler import SwarmRobotScheduler

        self.running = True
        self.robot_scheduler: SwarmRobotScheduler = swarm_robot_scheduler

        # Service calls
        self.ros_service_calls: SRSServiceCalls = SRSServiceCalls(
            self.robot_scheduler)

    def run(self):
        super().run()
        Log.info("Robot charging thread initialized.")
        try:
            while self.running:
                fleet_status: List[SwarmRobotStatus] = []
                available_robots: List[SwarmRobotStatus] = []
                robots_on_charging: List[SwarmRobotStatus] = []
                robots_for_charging: List[SwarmRobotStatus] = []
                if self.robot_scheduler.fleet_status is None:
                    Log.debug("Fleet status is not initialized.")
                else:
                    fleet_status = self.robot_scheduler.fleet_status.robots.copy()
                    for robot in fleet_status:
                        if robot.is_enabled is True and robot.order_assigned is False:
                            if robot.charging is True:
                                robots_on_charging.append(robot)
                            else:
                                available_robots.append(robot)

                    robots_for_charging = self.charge_robots_with_critical_battery(
                        available_robots)

                    if len(robots_on_charging) == 0:
                        self.charge_robots_with_low_battery(
                            robots_for_charging)

                time.sleep(self.robot_scheduler.config.get(
                    "robot_charge", "sleep_duration"))
        except:
            Log.exception_warning("Error in Robot charge.")

    def charge_robots_with_low_battery(self, robots: List[SwarmRobotStatus]) -> None:
        if len(robots):
            robots_without_pallet: List[SwarmRobotStatus] = []
            robots_with_pallet: List[SwarmRobotStatus] = []
            for robot in robots:
                if robot.is_loaded == True:
                    robots_with_pallet.append(robot)
                else:
                    robots_without_pallet.append(robot)

            if len(robots_without_pallet):
                low_battery_robot: SwarmRobotStatus = min(
                    robots_without_pallet, key=lambda robot: robot.battery_percentage)
            else:
                low_battery_robot: SwarmRobotStatus = min(
                    robots_with_pallet, key=lambda robot: robot.battery_percentage)
            self.send_robot_to_charging(low_battery_robot.id)

    def charge_robots_with_critical_battery(self, robots: List[SwarmRobotStatus]) -> List[SwarmRobotStatus]:
        robots_for_charging: List[SwarmRobotStatus] = []
        for robot in robots:
            battery_status_critical = self.robot_scheduler.config.get(
                "robot_charge", "battery_status_critical")
            battery_status_low = self.robot_scheduler.config.get(
                "robot_charge", "battery_status_low")
            if float(robot.battery_percentage) < battery_status_critical and \
                    robot.order_assigned is False:
                self.send_robot_to_charging(robot.id)
            elif float(robot.battery_percentage) < battery_status_low and \
                    robot.order_assigned is False:
                robots_for_charging.append(robot)
        return robots_for_charging

    def send_robot_to_charging(self, robot_id: str) -> bool:
        robot: RobotChargeRequest = RobotChargeRequest(robot_id)
        Log.info("Robot {0} requested charging".format(robot_id))
        return self.ros_service_calls.send_robot_to_charging(robot)

    def terminate(self):
        self.running = False
        Log.debug("(SRS) Terminate Robot Charge")
