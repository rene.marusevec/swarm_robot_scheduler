# system imports
from typing import Optional, List
import numpy as np

# self imports
from swarm_robot_scheduler_core.robot_charge import RobotCharge

# third party imports
from nobrainer.logger.log import Log
from nobrainer.ros.ros import ROS
from nobrainer.file.yamlconfig import YamlConfig
from swarm_msgs.msg import (FleetStatus, SwarmRobotStatus,
                            ItemPose)
from swarm_msgs.srv import (GetRobotForOrderResponse,
                            GetRobotForOrderRequest)
from std_msgs.msg import String, Bool, Float32
from geometry_msgs.msg import PoseStamped


class SwarmRobotScheduler:
    tags_metadata = [
        {
            "name": "Swarm Robot Scheduler",
            "description": "Main responsibility of this module is to select "
            + "the most appropriate robot for the requested order.",
        },
    ]

    def __init__(self, swarm_main_config_file: str, config_dir: str) -> None:
        self.ros: Optional[ROS] = None
        self.config: Optional[YamlConfig] = YamlConfig.instance(
            swarm_main_config_file,
            config_dir
        )
        self.fleet_status: FleetStatus = None

        Log.init(self.config)

        self.initialize_ros()

        # Provided services, publishers and subscribers
        from swarm_robot_scheduler_core.ros_registry import SRSRosRegistry
        self.ros_registry: SRSRosRegistry = SRSRosRegistry(self)
        self.ros_registry.register()
        Log.info("Services, publishers and subscribers are registered")

        # Initialize and start Robot Charge module
        if self.config.get("robot_charge", "is_enabled"):
            self.robot_charge_thread: RobotCharge = RobotCharge(self)
            self.robot_charge_thread.start()

        self.ros.spin()

        if self.config.get("robot_charge", "is_enabled"):
            self.robot_charge_thread.terminate()
            self.robot_charge_thread.join()

        self.terminate_ros()

    def request_is_not_complete(self, request: GetRobotForOrderRequest) -> Bool:
        response = None
        if request.order_id is None or \
                request.items[0].item_pose is None:
            response = True
            Log.debug("Request in get_robot is not complete.")
        else:
            response = False
        return response

    def get_l2_dist(self, robot_pose: PoseStamped,
                    item_pose: ItemPose) -> Float32:
        first_point = np.array((
            robot_pose.pose.position.x,
            robot_pose.pose.position.y,
            robot_pose.pose.position.z
        ))
        second_point = np.array((
            item_pose.position_x,
            item_pose.position_y,
            item_pose.position_z
        ))
        distance = np.linalg.norm(first_point - second_point)
        return distance

    def get_nearest_robot(self, available_robots: List[SwarmRobotStatus],
                          order_request: GetRobotForOrderRequest) -> String:
        robots_with_distances = []
        first_item = None
        for item in order_request.items:
            if item.item_sequence_number == 1:
                first_item = item
        
        if first_item is None:
            Log.exception_warning("Order {} doesn't have first item.".format(order_request.order_id))
            first_item = order_request.items[0]
        
        for robot in available_robots:
            distance_to_the_item = self.get_l2_dist(
                robot.robot_pose, first_item.item_pose)
            robot_with_distance = {
                'id': robot.id,
                'distance_to_item': distance_to_the_item
            }
            robots_with_distances.append(robot_with_distance)
        best_robot = min(robots_with_distances,
                         key=lambda robot: robot["distance_to_item"])
        Log.info("Robot {0} is assigned to order {1}".format(
            best_robot["id"], order_request.order_id))
        return best_robot["id"]

    def get_robot(self, request: GetRobotForOrderRequest) -> GetRobotForOrderResponse:
        """ Service callback for GetRobotForOrder """

        fleet_status: List[SwarmRobotStatus] = []
        available_robots: List[SwarmRobotStatus] = []
        available_robots_with_good_battery: List[SwarmRobotStatus] = []
        available_robots_with_low_battery: List[SwarmRobotStatus] = []
        available_robots_with_pallet: List[SwarmRobotStatus] = []

        try:
            Log.info("New request for GetRobotForOrder: {0}".format(
                request.order_id))
            if request is None:
                Log.debug("Request in get_robot is not valid.")
                return GetRobotForOrderResponse(
                    order_id=None,
                    robot_id=None
                )

            if self.request_is_not_complete(request):
                return GetRobotForOrderResponse(
                    order_id=request.order_id,
                    robot_id=None
                )

            if self.fleet_status is None:
                Log.debug("Fleet status is not initialized.")
                return GetRobotForOrderResponse(
                    order_id=request.order_id,
                    robot_id=None
                )

            fleet_status = self.fleet_status.robots.copy()
            for robot in fleet_status:
                if robot.is_enabled is True and \
                        robot.order_assigned is False and \
                        robot.charging is False and \
                        robot.status in ["EXECUTING_MISSION", "READY"] and \
                        robot.is_loaded is True:
                    available_robots.append(robot)

            for robot in available_robots:
                battery_status_low = self.config.get(
                    "robot_charge", "battery_status_low")
                battery_status_critical = self.config.get(
                    "robot_charge", "battery_status_critical")
                if float(robot.battery_percentage) > battery_status_low:
                    available_robots_with_good_battery.append(robot)
                if float(robot.battery_percentage) > battery_status_critical:
                    available_robots_with_low_battery.append(robot)

            if not len(available_robots_with_good_battery):
                available_robots_with_good_battery.extend(
                    available_robots_with_low_battery)

            if not len(available_robots_with_good_battery):
                Log.info("There is no available robots with good or low battery status and with pallet for order: {0}.".format(
                    request.order_id))
                return GetRobotForOrderResponse(
                    order_id=request.order_id,
                    robot_id=None
                )
            # for robot in available_robots_with_good_battery:
            #     if robot.is_loaded is True:
            #         available_robots_with_pallet.append(robot)

            # if not len(available_robots_with_pallet):
            #     # best_robot = self.get_nearest_robot(
            #     #     available_robots_with_good_battery, request)
            #     Log.info("There is no available robots with pallet for order: {0}.".format(
            #     request.order_id))
            #     return GetRobotForOrderResponse(
            #         order_id=request.order_id,
            #         robot_id=None
            #     )

            best_robot = self.get_nearest_robot(
                available_robots_with_good_battery, request)
            return GetRobotForOrderResponse(
                order_id=request.order_id,
                robot_id=best_robot
            )
        except:
            Log.exception_warning("Error in get_robot.")

    def get_fleet_status(self, swarm_fleet_status: FleetStatus) -> None:
        """ Subscriber callback for FleetStatus """
        self.fleet_status = swarm_fleet_status

    def robot_scheduler_status(self) -> String:
        """ Publisher callback for swarm_robot_scheduler_status """
        msg: String = String("Swarm Robot Scheduler is running.")
        if self.fleet_status is None:
            msg = String(
                "Swarm Robot Scheduler is running. Fleet status isn't initialized.")
        return msg

    def initialize_ros(self) -> None:
        self.ros = ROS(self.config.get("settings", "ros_node_name"))
        self.ros.init_node(anonymous=False)

    def terminate_ros(self) -> None:
        Log.debug("(SRS) Terminating ROS")
        self.ros.terminate_ros()
        self.ros.clean()
