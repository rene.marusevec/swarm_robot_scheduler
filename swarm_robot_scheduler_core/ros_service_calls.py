# third party imports
from nobrainer.logger.log import Log
from nobrainer.ros.ros import ROS, ServiceProxy
from swarm_msgs.srv import (RobotCharge, RobotChargeRequest,
                            RobotChargeResponse)


class SRSServiceCalls:
    """
    Swarm Robot Schduler Service Calls
    """

    def __init__(self, swarm_robot_scheduler: any) -> None:
        from swarm_robot_scheduler_core.swarm_robot_scheduler import SwarmRobotScheduler
        self.swarm_robot_scheduler: SwarmRobotScheduler = swarm_robot_scheduler

    def send_robot_to_charging(self, robot_for_charge: RobotChargeRequest) -> RobotChargeResponse:
        """
        Calls Swarm Engine service to sand Robot to charging
        """
        service_proxy: ServiceProxy = ROS.service_proxy(
            name=self.swarm_robot_scheduler.config.get(
                "calls", "robot_charge"),
            service_class=RobotCharge,
            wait_for_service=True,
            timeout=10
        )
        response: RobotChargeResponse = service_proxy.call(robot_for_charge)

        if response is None:
            Log.warning("Robot: {0} is not charged because service " +
                        "is unavailable.".format(robot_for_charge))
        elif not response:
            Log.debug("Robot: {0} isn't sent to charging".format(
                robot_for_charge))
        else:
            Log.info("Robot: {0} is sent to charging".format(robot_for_charge))

        return response
