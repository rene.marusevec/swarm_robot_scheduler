# self imports
from swarm_robot_scheduler_core.swarm_robot_scheduler import SwarmRobotScheduler

# third party imports
from nobrainer.ros.registry import Registry
from swarm_msgs.msg import FleetStatus
from swarm_msgs.srv import GetRobotForOrder
from std_msgs.msg import String


class SRSRosRegistry(Registry):
    def __init__(self, swarm_robot_scheduler: SwarmRobotScheduler):
        """ Settings for ros services, subscibers and publishers """
        super().__init__()

        self.robot_scheduler: SwarmRobotScheduler = swarm_robot_scheduler

        self.ros_subscribers = [
            {
                "name": self.robot_scheduler.config.get("topics", "swarm_fleet_status"),
                "data_class": FleetStatus,
                "callback": self.robot_scheduler.get_fleet_status,
                "queue_size": 10,
                "callback_args": None
            }
        ]
        self.ros_publishers = [
            {
                "name": self.robot_scheduler.config.get("topics", "swarm_robot_scheduler_status"),
                "data_class": String,
                "rate": 1.0,
                "message_generator": self.robot_scheduler.robot_scheduler_status,
                "queue_size": 10,
                "latch": False
            }
        ]
        self.ros_services = [
            {
                "name": self.robot_scheduler.config.get("services", "get_robot_for_order"),
                "service_class": GetRobotForOrder,
                "handler": self.robot_scheduler.get_robot
            }
        ]
