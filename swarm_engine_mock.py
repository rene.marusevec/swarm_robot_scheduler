import time
from nobrainer.logger.log import Log
from std_msgs.msg import String
from nobrainer.ros.ros import ServiceProxy, ThreadedPublisher, ROS, Service

from typing import List, Type, TypedDict, Text
from std_msgs.msg import String
from swarm_msgs.msg import FleetStatus, ItemMsg, ItemPose, SwarmRobotStatus
from swarm_msgs.srv import (RobotCharge, RobotChargeResponse, RobotChargeRequest,
                            GetRobotForOrder, GetRobotForOrderRequest, GetRobotForOrderResponse)
from geometry_msgs.msg import PoseStamped


def swarm_fleet_status_msg() -> FleetStatus:
    fleet_status: FleetStatus = FleetStatus()

    for robot in test_robots_data:
        if robot["battery_percentage"] > 0:
            robot["battery_percentage"] = robot["battery_percentage"] - 0.1
        robot_status = SwarmRobotStatus()
        robot_status.ip_address = "<IP>:8888/fleet"
        robot_status.name = robot["name"]
        robot_status.status = "READY" # "EXECUTING_MISSION"
        robot_status.is_enabled = True
        robot_status.id = robot["id"]
        robot_status.order_assigned = robot["order_assigned"]
        robot_status.order_id = robot["order_id"]
        robot_status.battery_percentage = str(
            round(robot["battery_percentage"], 2))
        robot_status.is_loaded = robot["is_loaded"]
        robot_status.charging = robot["charging"]

        fleet_status.robots.append(robot_status)

    return fleet_status


def publish_swarm_fleet_status() -> ThreadedPublisher:

    threaded_publisher: ThreadedPublisher = ROS.threaded_publisher(
        name="/swarm_engine/fleet/status",
        data_class=FleetStatus,
        rate=0.5,
        message_generator=swarm_fleet_status_msg
    )

    threaded_publisher.start()

    return threaded_publisher


def get_robot_for_order(order: GetRobotForOrderRequest) -> GetRobotForOrderResponse:
    """
        Calls Swarm Robot Scheduler service to get Robot for Order
    """
    try:
        sample_service: ServiceProxy = ROS.service_proxy(
            "/swarm_robot_scheduler/get_robot_for_order",
            GetRobotForOrder,
            wait_for_service=True,
            timeout=60
        )
        response: GetRobotForOrderResponse = sample_service(order)
        if response == None:
            Log.warning("Fail")
        else:
            print("get_robot_for_order: {0}".format(response))
            Log.info("Success")

    except Exception:
        Log.exception_warning("Service call failed!")


def robot_charged(robot_id: str):
    for robot in test_robots_data:
        if robot_id == robot["id"]:
            robot["charging"] = False
            # TODO error battery percentage is not updated
            robot["battery_percentage"] = 99,
            print("Robot {0} is charged.".format(robot_id))


def robot_charge_handler(request: RobotChargeRequest) -> RobotChargeResponse:
    print("robot_charge_handler: {0}".format(request))
    for robot in test_robots_data:
        if request.robot_id == robot["id"]:
            robot["charging"] = True
            # robot_charged(request.robot_id)

    response: RobotChargeResponse = RobotChargeResponse()
    if request == None:
        response.success = False
        return RobotChargeResponse(False)
    else:
        response.success = True
        return RobotChargeResponse(True)


def test_robot_status_data1() -> list():
    list_of_robots = list()
    gideon_1 = {
        "name": "gideon1",
        "id": "gideon1",
        "order_assigned": False,
        "order_id": "",
        "battery_percentage": 19,
        "is_loaded": False,
        "charging": False 
    }
    gideon_2 = {
        "name": "gideon2",
        "id": "gideon2",
        "order_assigned": False,
        "order_id": "",
        "battery_percentage": 55,
        "is_loaded": False,
        "charging": False 
    }
    gideon_3 = {
        "name": "gideon3",
        "id": "gideon3",
        "order_assigned": False,
        "order_id": "order_id_3",
        "battery_percentage": 15,
        "is_loaded": True,
        "charging": False 
    }
    gideon_4 = {
        "name": "gideon4",
        "id": "gideon4",
        "order_assigned": False,
        "order_id": "",
        "battery_percentage": 80,
        "is_loaded": False,
        "charging": False 
    }
    list_of_robots.append(gideon_1)
    list_of_robots.append(gideon_2)
    list_of_robots.append(gideon_3)
    list_of_robots.append(gideon_4)

    return list_of_robots


def test_robot_status_data2() -> list():
    list_of_robots = list()
    gideon_1 = {
        "name": "gideon1",
        "id": "gideon1",
        "order_assigned": False,
        "order_id": "",
        "battery_percentage": 19,
        "is_loaded": True,
        "charging": False
    }
    gideon_2 = {
        "name": "gideon2",
        "id": "gideon2",
        "order_assigned": False,
        "order_id": "",
        "battery_percentage": 55,
        "is_loaded": False,
        "charging": False
    }
    gideon_3 = {
        "name": "gideon3",
        "id": "gideon3",
        "order_assigned": True,
        "order_id": "order_id_3",
        "battery_percentage": 15,
        "is_loaded": True,
        "charging": False
    }
    gideon_4 = {
        "name": "gideon4",
        "id": "gideon4",
        "order_assigned": False,
        "order_id": "",
        "battery_percentage": 80,
        "is_loaded": False,
        "charging": False
    }
    list_of_robots.append(gideon_1)
    list_of_robots.append(gideon_2)
    list_of_robots.append(gideon_3)
    list_of_robots.append(gideon_4)

    return list_of_robots


def test_robot_status_data3() -> list():
    list_of_robots = list()
    gideon_1 = {
        "name": "gideon1",
        "id": "gideon1",
        "order_assigned": False,
        "order_id": "",
        "battery_percentage": 19,
        "is_loaded": False,
        "charging": False
    }
    gideon_2 = {
        "name": "gideon2",
        "id": "gideon2",
        "order_assigned": False,
        "order_id": "",
        "battery_percentage": 55,
        "is_loaded": False,
        "charging": False
    }
    gideon_3 = {
        "name": "gideon3",
        "id": "gideon3",
        "order_assigned": False,
        "order_id": "order_id_3",
        "battery_percentage": 5,
        "is_loaded": True,
        "charging": False
    }
    gideon_4 = {
        "name": "gideon4",
        "id": "gideon4",
        "order_assigned": False,
        "order_id": "",
        "battery_percentage": 80,
        "is_loaded": False,
        "charging": False
    }
    list_of_robots.append(gideon_1)
    list_of_robots.append(gideon_2)
    list_of_robots.append(gideon_3)
    list_of_robots.append(gideon_4)

    return list_of_robots


def test_robot_status_data4() -> list():
    list_of_robots = list()
    gideon_1 = {
        "name": "gideon1",
        "id": "gideon1",
        "order_assigned": False,
        "order_id": "",
        "battery_percentage": 19,
        "is_loaded": False,
        "charging": False
    }
    gideon_2 = {
        "name": "gideon2",
        "id": "gideon2",
        "order_assigned": False,
        "order_id": "",
        "battery_percentage": 55,
        "is_loaded": False,
        "charging": False
    }
    gideon_3 = {
        "name": "gideon3",
        "id": "gideon3",
        "order_assigned": False,
        "order_id": "order_id_3",
        "battery_percentage": 15,
        "is_loaded": False,
        "charging": True
    }
    gideon_4 = {
        "name": "gideon4",
        "id": "gideon4",
        "order_assigned": False,
        "order_id": "",
        "battery_percentage": 80,
        "is_loaded": False,
        "charging": False
    }
    list_of_robots.append(gideon_1)
    list_of_robots.append(gideon_2)
    list_of_robots.append(gideon_3)
    list_of_robots.append(gideon_4)

    return list_of_robots


if __name__ == '__main__':

    ros: ROS = ROS("swarm_engine")

    # select different test cases
    test_robots_data = test_robot_status_data1()
    # test_robots_data = test_robot_status_data2()
    # test_robots_data = test_robot_status_data3()
    # test_robots_data = test_robot_status_data4()

    # Initialization of the node
    ros.init_node()

    # Publishers
    swarm_fleet_status_publisher: ThreadedPublisher = publish_swarm_fleet_status()

    time.sleep(4)
    # Client - Service call
    item_pose = ItemPose()
    item_pose.position_x = 1
    item_pose.position_y = 2

    item1 = ItemMsg()
    item1.item_id = "item1"
    item1.item_poi_name = "position1"
    item1.item_pose = item_pose
    item1.item_sequence_number = 1

    item2 = ItemMsg()
    item2.item_id = "item2"
    item2.item_poi_name = "position2"
    item2.item_pose = item_pose
    item2.item_sequence_number = 2

    get_robot_request: GetRobotForOrderRequest = GetRobotForOrderRequest()
    get_robot_request.order_id = "order1"
    get_robot_request.items.append(item1)
    get_robot_request.items.append(item2)
    get_robot_for_order(get_robot_request)

    # Simple service
    service: Service = ROS.service(
        name="/swarm_engine/robot_charge",
        service_class=RobotCharge,
        handler=robot_charge_handler
    )

    # Start the ROS spin loop so your program does not end - this will block executing any code below
    ros.spin()

    # Stop all publishers
    swarm_fleet_status_publisher.stop()

    ros.terminate_ros()
    # In the end, don't forget to simply stop and remove all publishers by simply calling a ROS function
    ROS.clean()
