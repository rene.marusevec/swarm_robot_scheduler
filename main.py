from swarm_robot_scheduler_core.swarm_robot_scheduler import SwarmRobotScheduler

if __name__ == '__main__':
    swarm_robot_scheduler: SwarmRobotScheduler = SwarmRobotScheduler(
        "swarm_robot_scheduler.yaml", "config/swarm/robot_scheduler")
